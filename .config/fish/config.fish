#      _____   ____ ______
#     /     | /   //  __  \     Nicolei Rademacher
#    /      |/   //  /_/  /     nicolei@posteo.de
#   /   /|  |   //     __/      2020-08-24
#  /   / |     //  /\  \
# /___/  |____//__/  \__\
#
#


set -x PATH $PATH /opt/linuxtrack/bin ~/Games/X-Plane\ 11/
set -U EDITOR vim
set fish_greeting 

### ALIASES ###

## navigation
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

## vim and emacs
# alias vim="nvim"
# alias em="/usr/bin/emacs -nw"
# alias emacs="emacsclient -c -a 'emacs'"

## git dotfiles
alias config='/usr/bin/git --git-dir=$HOME/dotclumsynick --work-tree=$HOME'

## pacman and yay
alias pacsyu='sudo pacman -Syyu'                 # update only standard pkgs
alias yaysua="yay -Sua --noconfirm"              # update only AUR pkgs
alias yaysyu="yay -Syu --noconfirm"              # update standard pkgs and AUR pkgs
alias unlock="sudo rm /var/lib/pacman/db.lck"    # remove pacman lock
# alias cleanup='sudo pacman -Rns $(pacman -Qtdq)' # remove orphaned packages

## Changing "ls" to "exa"
alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing
alias l.='exa -a | egrep "^\."'


## confirm before overwriting something
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'

## adding flags
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB
alias lynx='lynx -cfg=~/.lynx/lynx.cfg -lss=~/.lynx/lynx.lss -vikeys'
alias vifm='./.config/vifm/scripts/vifmrun'


## get error messages from journalctl
alias jctl="journalctl -p 3 -xb"
