#!/usr/bin/env bash

function run {
	if ! pgrep -f $1 ;
	then
	$@&
	fi
}


run setxkbmap -layout "de,us"
run picom --config ~/.config/picom/picom.conf
run nitrogen --restore
run nextcloud
